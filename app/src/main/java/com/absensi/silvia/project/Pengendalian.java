package com.absensi.silvia.project;

/**
 * Created by silvia on 4/22/2018.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by silvia on 4/14/2018.
 */

public class Pengendalian extends AppCompatActivity {

    ListView listView;
    int gambar [] = {R.drawable.belangdaun,
            R.drawable.akars,
            R.drawable.jamurupas,R.drawable.kankerbatang,
            R.drawable.moniliasis,R.drawable.vsd, R.drawable.busukbuah};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pengendalian);

        listView = (ListView) findViewById(R.id.list);
        String[] values = new String[]{"Pengendalian : Antraknose", "Pengendalian : Penggerek Batang",
                "Pengendalian : Penggerek Buah", "Pengendalian : Kumbang Apoginia Sp","Pengendalian : Tikus",
                "Pengendalian : Jamur Akar","Pengendalian : Jamur Upas","Pengendalian : Vascular Streak Dieback","Pengendalian : Kepik Penghisap Buah","Pengendalian : Busuk Buah",
                "Pengendalian : Moniliasis","Pengendalian : Ulat","Pengendalian : Kanker Batang","Pengendalian : Belang Daun"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (position == 0) {
                    Intent myIntent = new Intent(view.getContext(), Quest1.class);
                    startActivityForResult(myIntent, 0);
                }

                if (position == 1) {
                    Intent myIntent = new Intent(view.getContext(), Quest2.class);
                    startActivityForResult(myIntent, 0);
                }

                if (position == 2) {
                    Intent myIntent = new Intent(view.getContext(), Quest3.class);
                    startActivityForResult(myIntent, 0);
                }

                if (position == 3) {
                    Intent myIntent = new Intent(view.getContext(), Quest5.class);
                    startActivityForResult(myIntent, 0);
                }

                if (position == 4) {
                    Intent myIntent = new Intent(view.getContext(), Quest6.class);
                    startActivityForResult(myIntent, 0);
                }

                if (position == 5) {
                    Intent myIntent = new Intent(view.getContext(), Quest7.class);
                    startActivityForResult(myIntent, 0);
                }

                if (position == 6) {
                    Intent myIntent = new Intent(view.getContext(), Quest17.class);
                    startActivityForResult(myIntent, 0);
                }
                if (position == 7) {
                    Intent myIntent = new Intent(view.getContext(), Quest41.class);
                    startActivityForResult(myIntent, 0);
                }
                if (position == 8) {
                    Intent myIntent = new Intent(view.getContext(), Quest69.class);
                    startActivityForResult(myIntent, 0);
                }
                if (position == 9) {
                    Intent myIntent = new Intent(view.getContext(), Questg1.class);
                    startActivityForResult(myIntent, 0);
                }
                if (position == 10) {
                    Intent myIntent = new Intent(view.getContext(), Monili.class);
                    startActivityForResult(myIntent, 0);
                }
                if (position == 11) {
                    Intent myIntent = new Intent(view.getContext(), Ulat.class);
                    startActivityForResult(myIntent, 0);
                }
                if (position == 12) {
                    Intent myIntent = new Intent(view.getContext(), Kankerba.class);
                    startActivityForResult(myIntent, 0);
                }
                if (position == 13) {
                    Intent myIntent = new Intent(view.getContext(), Belang.class);
                    startActivityForResult(myIntent, 0);
                }
//
//                if (position == 7) {
//                    Intent myIntent = new Intent(view.getContext(), ListItemActivity2.class);
//                    startActivityForResult(myIntent, 0);
//                }
            }
        });
    }
}
