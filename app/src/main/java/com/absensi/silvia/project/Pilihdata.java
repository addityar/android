package com.absensi.silvia.project;

/**
 * Created by silvia on 2/18/2018.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by silvia on 2/3/2018.
 */


public class Pilihdata extends Activity {


    Button btnA, btnB,btnC,btnD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pilih_data);


        btnA = (Button) findViewById(R.id.lokal);
        btnB = (Button) findViewById(R.id.sistemik);
        btnC = (Button) findViewById(R.id.back);
        btnD = (Button) findViewById(R.id.solusi);

        btnA.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Pilihdata.this, MainActivity.class);
                startActivity(intent);
            }
        });
//
//
        btnB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Pilihdata.this, ListViewItemClickEventExample.class);
                startActivity(intent);
            }
        });

        btnC.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Pilihdata.this, Utama.class);
                startActivity(intent);
            }
        });

        btnD.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Pilihdata.this, Pengendalian.class);
                startActivity(intent);
            }
        });

    }
}