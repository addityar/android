package com.absensi.silvia.project;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

/**
 * Created by silvia on 4/19/2018.
 */

public class Gejaladaun extends Activity implements View.OnClickListener {

    Button hasil, selesai, lihat,so1,so2,so3,so4,so5,so6,so7,so8,so9,so10,so11,so12,so13,so14;
    EditText persentasi1, persentasi2, persentasi3, persentasi4, persentasi5, persentasi6, persentasi7,
            persentasi8, persentasi9, persentasi10, persentasi11, persentasi12, persentasi13, persentasi14;
    CheckBox a, b, c, d, e, f,g,h,i,j,k;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gdaun);

        a = (CheckBox) findViewById(R.id.cb1);
        b = (CheckBox) findViewById(R.id.cb2);
        c = (CheckBox) findViewById(R.id.cb3);
        d = (CheckBox) findViewById(R.id.cb4);
        e = (CheckBox) findViewById(R.id.cb5);
        f = (CheckBox) findViewById(R.id.cb6);
//        g = (CheckBox) findViewById(R.id.cb7);
//        h = (CheckBox) findViewById(R.id.cb8);
//        i = (CheckBox) findViewById(R.id.cb9);
//        j = (CheckBox) findViewById(R.id.cb10);

        persentasi1 = (EditText) findViewById(R.id.persentase1);
        persentasi2 = (EditText) findViewById(R.id.persentase2);
        persentasi3 = (EditText) findViewById(R.id.persentase3);
        persentasi4 = (EditText) findViewById(R.id.persentase4);
        persentasi5 = (EditText) findViewById(R.id.persentase5);
        persentasi6 = (EditText) findViewById(R.id.persentase6);
        persentasi7 = (EditText) findViewById(R.id.persentase7);
        persentasi8 = (EditText) findViewById(R.id.persentase8);
        persentasi9 = (EditText) findViewById(R.id.persentase9);
//        persentasi10 = (EditText) findViewById(R.id.persentase10);
//        persentasi11 = (EditText) findViewById(R.id.persentase11);
//        persentasi12 = (EditText) findViewById(R.id.persentase12);
        persentasi13 = (EditText) findViewById(R.id.persentase13);
        persentasi14 = (EditText) findViewById(R.id.persentase14);


        hasil = (Button) findViewById(R.id.hasil);
        hasil.setOnClickListener(this);

    }

    @Override
    public void onClick(View klik) {
        // TODO Auto-generated method stub


        if (a.isChecked() && b.isChecked()
                && c.isChecked() && d.isChecked() && e.isChecked() && f.isChecked()) {
            int ulat = (int)((8.0/10.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));


        } else if (a.isChecked() && b.isChecked()
                && c.isChecked() && d.isChecked() && e.isChecked()) {
            int ulat = (int)((8.0/9.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()
                && c.isChecked() && d.isChecked()) {
            int ulat = (int)((8.0/8.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()
                && c.isChecked()) {
            int ulat = (int)((3.0/4.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()) {
            int ulat = (int)((2.0/4.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked()) {
            int ulat = (int)((1.0/4.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));


        } else if (b.isChecked()) {
            int ulat = (int)((1.0/4.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));


        } else if (c.isChecked()) {
            int ulat = (int)((1.0/4.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked()) {
            int ulat = (int)((5.0/10.0)*100);
            int kepik = (0);
            int kumbang = (int)((5.0/10.0)*100);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (e.isChecked()) {
            int ulat = (int)((1.0/2.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (f.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (int)((1.0/2.0)*100);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()&& d.isChecked()) {
            int ulat = (int)((7.0/12.0)*100);
            int kepik = (0);
            int kumbang = (int)((5.0/12.0)*100);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked() && e.isChecked()
                ) {
            int ulat = (int)((2.0/3.0)*100);
            int kepik = (0);
            int kumbang = (int)((1.0/3.0)*100);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked() && f.isChecked()
                ) {
            int ulat = (int)((2.0/3.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (int)((1.0/3.0)*100);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (b.isChecked() && c.isChecked() && d.isChecked() && e.isChecked() && f.isChecked()
                ) {
            int ulat = (int)((7.0/9.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked() && c.isChecked()
                && e.isChecked()&& f.isChecked()) {
            int ulat = (int)((6.0/8.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked() && e.isChecked()
                && f.isChecked()) {
            int ulat = (int)((5.0/7.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (f.isChecked() && e.isChecked()
                ) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (int)((1.0/2.0)*100);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && c.isChecked()
                ) {
            int ulat = (int)((2.0/4.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && b.isChecked()
                && d.isChecked()) {
            int ulat = (int)((7.0/12.0)*100);
            int kepik = (0);
            int kumbang = (int)((5.0/12.0)*100);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && b.isChecked()
                && e.isChecked()) {
            int ulat = (int)((2.0/3.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked() && b.isChecked()
                && c.isChecked()) {
            int ulat = (int)((7.0/12.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && b.isChecked()
                && f.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (int)((1.0/3.0)*100);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && b.isChecked())
                 {
                     int ulat = (int)((2.0/4.0)*100);
                     int kepik = (0);
                     int kumbang = (0);
                     int pbatang = (0);
                     int pbuah = (0);
                     int tikus = (0);
                     int antraknose = (0);
                     int vsd = (int)(0);
                     int bbuah = (0);
                     int jupas = (0);
                     int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && f.isChecked()
                ) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (int)((1.0/2.0)*100);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && e.isChecked()
                ) {
            int ulat = (int)((1.0/2.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked() && d.isChecked()&& e.isChecked() && f.isChecked()) {
            int ulat = (int)((7.0/9.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (int)((1.0/9.0)*100);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && d.isChecked() && e.isChecked() && f.isChecked()) {
            int ulat = (int)((6.0/8.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (int)((1.0/8.0)*100);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked() && e.isChecked() && a.isChecked()) {
            int ulat = (int)((6.0/7.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if ( a.isChecked() && b.isChecked() && e.isChecked()) {
            int ulat = (int)((2.0/4.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (int)((1.0/8.0)*100);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (b.isChecked()&& a.isChecked()&& d.isChecked()) {
            int ulat = (int)((7.0/12.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);




            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (b.isChecked()&& f.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (int)((1.0/2.0)*100);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked()&& b.isChecked()&& f.isChecked()) {
            int ulat = (int)((2.0/3.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (int)((1.0/3.0)*100);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked()&& d.isChecked()) {
            int ulat = (int)((6.0/12.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked()&& b.isChecked()) {
            int ulat = (int)((6.0/11.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (e.isChecked()&& a.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (int)((1.0/2.0)*100);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (f.isChecked()&& b.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (int)((1.0/2.0)*100);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked()&& b.isChecked()&& f.isChecked()) {
            int ulat = (int)((2.0/3.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (int)((1.0/3.0)*100);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (e.isChecked()&& b.isChecked()&& d.isChecked()) {
            int ulat = (int)((6.0/12.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked()&& b.isChecked()&& e.isChecked()) {
            int ulat = (int)((2.0/3.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (b.isChecked()&& e.isChecked()&& d.isChecked()) {
            int ulat = (int)((6.0/12.0)*100);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (int)(0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));


        }


        a.setChecked(false);
        b.setChecked(false);
        c.setChecked(false);
        d.setChecked(false);
        e.setChecked(false);
        f.setChecked(false);
        so1 = (Button) findViewById(R.id.s1);
        so2 = (Button) findViewById(R.id.s2);
        so3 = (Button) findViewById(R.id.s3);
        so4 = (Button) findViewById(R.id.s4);
        so5 = (Button) findViewById(R.id.s5);
        so6 = (Button) findViewById(R.id.s6);
        so7 = (Button) findViewById(R.id.s7);
        so8 = (Button) findViewById(R.id.s8);
        so9 = (Button) findViewById(R.id.s9);
        so10 = (Button) findViewById(R.id.s10);
        so11 = (Button) findViewById(R.id.s11);
        so12 = (Button) findViewById(R.id.s12);
        so13 = (Button) findViewById(R.id.s13);
        so14 = (Button) findViewById(R.id.s14);

//        selesai.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Gejalabatang.this, Utama.class);
//                startActivity(intent);
//
//            }
//        });
//        lihat.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Gejalabatang.this, Pengendalian.class);
//                startActivity(intent);
//
//            }
//        });
        so1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejaladaun.this, Ulat.class);
                startActivity(intent);

            }
        });
        so2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejaladaun.this, Quest69.class);
                startActivity(intent);

            }
        });
        so3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejaladaun.this, Quest5.class);
                startActivity(intent);

            }
        });
        so4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejaladaun.this, Quest2.class);
                startActivity(intent);

            }
        });
        so5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejaladaun.this, Quest3.class);
                startActivity(intent);

            }
        });
        so6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejaladaun.this, Quest6.class);
                startActivity(intent);

            }
        });
        so7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejaladaun.this, Quest41.class);
                startActivity(intent);

            }
        });
        so8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejaladaun.this, Quest17.class);
                startActivity(intent);

            }
        });
        so9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejaladaun.this, Quest7.class);
                startActivity(intent);

            }
        });
//        so10.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Gejaladaun.this, Monili.class);
//                startActivity(intent);
//
//            }
//        });
//        so11.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Gejaladaun.this, Belang.class);
//                startActivity(intent);
//
//            }
//        });
//        so12.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Gejaladaun.this, Kankerba.class);
//                startActivity(intent);
//
//            }
//        });
        so13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejaladaun.this, Quest1.class);
                startActivity(intent);

            }
        });
        so14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejaladaun.this, Questg1.class);
                startActivity(intent);

            }
        });

    }
}
