package com.absensi.silvia.project;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by silvia on 4/12/2018.
 */

public class Database extends SQLiteOpenHelper {
    final static String DB_NAME = "db_kuis";

    public Database(Context context) {
        super(context, DB_NAME, null, 1);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS tbl_soal(id INTEGER PRIMARY KEY AUTOINCREMENT, soal TEXT, pil_a TEXT, pil_b TEXT, pil_c TEXT, jwban INTEGER, img BLOB)";
        db.execSQL(sql);

        ContentValues values = new ContentValues();
        values.put("soal", "Apakah terdapat lubang pada tengah daun?");
        values.put("pil_a", "Ya");
        values.put("jwban","0.1");
        values.put("img", R.drawable.lubangdaun);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah daun berbintik transparan?");
        values.put("pil_a", "Ya");
        values.put("jwban","0.2");
        values.put("img", R.drawable.cacao);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah daun menguning?");
        values.put("pil_a", "Ya");
        values.put("jwban","0.3");
        values.put("img", R.drawable.daunkuning);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah terdapat bercak pada daun?");
        values.put("pil_a", "Ya");
        values.put("jwban","0.3");
        values.put("img", R.drawable.bercakhijau);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah daun rontok?");
        values.put("pil_a", "Ya");
        values.put("jwban","0.2");
        values.put("img", R.drawable.daunkering);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah daun layu?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.2");
        values.put("img", R.drawable.daunlayu);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah terdapat bintik / bercak coklat tidak teratur pada daun muda?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.5");
        values.put("img", R.drawable.bercakcoklat);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah terdapat bercak nekrosis tidak teratur pada daun tua?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.5");
        values.put("img", R.drawable.nekrosis);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah tanaman mengalami belang daun?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.4");
        values.put("img", R.drawable.belangdaun);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah pinggir daun berlubang?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.5");
        values.put("img", R.drawable.daunpinggir);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah ranting atau cabang layu dan mengering?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.2");
        values.put("img", R.drawable.cacao);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah tunas muda membentuk ranting pendek yang agak membengkak?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.3");
        values.put("img", R.drawable.bengkak);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah daun layu atau kering tetap melekat pada cabang?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.4");
        values.put("img", R.drawable.melekat);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah tanaman pada ranting tanpa daun?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.5");
        values.put("img", R.drawable.daunkering);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah terdapa 3 bintik coklat apabila ranting / cabang diiris tipis?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.6");
        values.put("img", R.drawable.vsd);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah permukaan kulit ranting / cabang kasar dan belang ?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.6");
        values.put("img", R.drawable.kasar);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah jika ranting dibelah membujur tampak seperti garis-garis coklat?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.2");
        values.put("img", R.drawable.garis);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah ranting mengalami mati pucuk?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.3");
        values.put("img", R.drawable.matipucuk);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah pada batang terdapat lubang gerekan berbentuk cincin?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.6");
        values.put("img", R.drawable.buih);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah terdapat kotoran dengan serpihan pada lubang gerekan?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.6");
        values.put("img", R.drawable.kotoran);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah terdapat sisa gerekan berserat dan berbuih?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.6");
        values.put("img", R.drawable.buih);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah batang menggembung?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.5");
        values.put("img", R.drawable.cacao);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah batang berwarna gelap kehitaman, retak, busuk serta basah?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.4");
        values.put("img", R.drawable.gelap);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah terdapat cairan kemerahan seperti karat?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.4");
        values.put("img", R.drawable.cairan);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah jika kulit luar dibersihkan tampak lapisan busuk berwarna coklat?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.3");
        values.put("img", R.drawable.kankerbatang);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah buah muda keriput?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.5");
        values.put("img", R.drawable.keriput);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah buah muda berbintik coklat berlekuk?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.4");
        values.put("img", R.drawable.berlekuk);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah busuk kering pada ujung buah?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.3");
        values.put("img", R.drawable.buahtua);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah buah muda mengering?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.2");
        values.put("img", R.drawable.buahkering);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah bercak / titik coklat cekung kehitaman pada buah tua?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.2");
        values.put("img", R.drawable.titikcoklat);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah permukaan kulit buah muda retak?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.5");
        values.put("img", R.drawable.retak);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah terjadi perubahan bentuk pada buah muda?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.3");
        values.put("img", R.drawable.berlekuk);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah terjadi busuk buah sebagian?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.3");
        values.put("img", R.drawable.busukbuah);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah buah berlubang dan rusak?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.6");
        values.put("img", R.drawable.lubangbuah);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah biji buah berceceran?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.7");
        values.put("img", R.drawable.cacao);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah ujung buah berwarna coklat?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","0.4");
        values.put("img", R.drawable.buahcoklat);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah buah berwarna kehitaman?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","1");
        values.put("img", R.drawable.kehitaman);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah buah lembek dan basah?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","1");
        values.put("img", R.drawable.cacao);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah pada buah timbuk lapisan jamur seperti tepung?");
        values.put("pil_a", "Ya");
//        values.put("pil_b","Tidak");
//        values.put("pil_c", "Niphelium lappaceum L.");
        values.put("jwban","1");
        values.put("img", R.drawable.berjamur);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah terdapat lubang-lubang kecil pada buah?");
        values.put("pil_a", "Ya");
        values.put("jwban","1");
        values.put("img", R.drawable.lubangkecil);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah buah tua tidak berbunyi jika digoyangkan?");
        values.put("pil_a", "Ya");
        values.put("jwban","1");
        values.put("img", R.drawable.buahtua);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah biji buah saling melekat berwarna kehitaman?");
        values.put("pil_a", "Ya");
        values.put("jwban","1");
        values.put("img", R.drawable.melekatb);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah ukuran biji buah lebih kecil?");
        values.put("pil_a", "Ya");
        values.put("jwban","1");
        values.put("img", R.drawable.hama);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah buah muda yg masih berukuran 2cm membengkak dan mati?");
        values.put("pil_a", "Ya");
        values.put("jwban","1");
        values.put("img", R.drawable.bengkakmati);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah terdapat bercak keras gelap pada buag tua?");
        values.put("pil_a", "Ya");
        values.put("jwban","1");
        values.put("img", R.drawable.bercakkeras);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah biji buah berubah menjadi massa berlendir?");
        values.put("pil_a", "Ya");
        values.put("jwban","1");
        values.put("img", R.drawable.berlendir);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah terdapat bercak berwarna buah masak pada kulit buah?");
        values.put("pil_a", "Ya");
        values.put("jwban","1");
        values.put("img", R.drawable.masak);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah bercak coklat diliputi jamur berwarna putih dan kotor pada buah?");
        values.put("pil_a", "Ya");
        values.put("jwban","1");
        values.put("img", R.drawable.moniliasis);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah biji buah busuk dan hancur?");
        values.put("pil_a", "Ya");
        values.put("jwban","1");
        values.put("img", R.drawable.hancur);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah jika buah mentah dibelah membuujur terdapat garis coklat / kelabu / hitam?");
        values.put("pil_a", "Ya");
        values.put("jwban","1");
        values.put("img", R.drawable.cacao);
        db.insert("tbl_soal", "soal", values);

        values.put("soal", "Apakah terdapat benang-benang jamur pada permukaan akar?");
        values.put("pil_a", "Ya");
        values.put("jwban","1");
        values.put("img", R.drawable.akars);
        db.insert("tbl_soal", "soal", values);


    }

    public List<Soal> getSoal(){
        List<Soal> listSoal = new ArrayList<Soal>();
        String query = "select * from tbl_soal";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do{
                Soal s = new Soal();
                s.setSoal(cursor.getString(1));
                s.setPil_a(cursor.getString(2));
//                s.setPil_b(cursor.getString(3));
//                s.setPil_c(cursor.getString(4));
                s.setJwban(cursor.getInt(5));
                s.setGambar(cursor.getInt(6));
                listSoal.add(s);
            }while(cursor.moveToNext());
        }

        return listSoal;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS tbl_soal");
        onCreate(db);

    }

}
