package com.absensi.silvia.project;

/**
 * Created by silvia on 2/18/2018.
 */


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by silvia on 2/3/2018.
 */


public class Hasil1 extends Activity {
    EditText persentasi1,persentasi2,persentasi3,persentasi4,persentasi5,persentasi6,persentasi7,
            persentasi8,persentasi9,persentasi10,persentasi11,persentasi12,persentasi13,persentasi14;
    Button lihatdata,finish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hasil1);

        persentasi1=(EditText) findViewById(R.id.persentase1);
        persentasi2=(EditText) findViewById(R.id.persentase2);
        persentasi3=(EditText) findViewById(R.id.persentase3);
        persentasi4=(EditText) findViewById(R.id.persentase4);
        persentasi5=(EditText) findViewById(R.id.persentase5);
        persentasi6=(EditText) findViewById(R.id.persentase6);
        persentasi7=(EditText) findViewById(R.id.persentase7);
        persentasi8=(EditText) findViewById(R.id.persentase8);
        persentasi9=(EditText) findViewById(R.id.persentase9);
        persentasi10=(EditText) findViewById(R.id.persentase10);
        persentasi11=(EditText) findViewById(R.id.persentase11);
        persentasi12=(EditText) findViewById(R.id.persentase12);
        persentasi13=(EditText) findViewById(R.id.persentase13);
        persentasi14=(EditText) findViewById(R.id.persentase14);
        lihatdata=(Button) findViewById(R.id.lihat);
        finish=(Button) findViewById(R.id.selesai);
        lihatdata.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Hasil1.this, Pilihdata.class);
                startActivity(intent);
            }
        });
//
//
        finish.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Hasil1.this, Utama.class);
                startActivity(intent);
            }
        });


    }
}