package com.absensi.silvia.project;

/**
 * Created by silvia on 4/1/2018.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by silvia on 2/3/2018.
 */


public class Penyakits extends Activity {


    Button btnA, btnB,btnC,btnD,btnE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.penyakits);


        btnA = (Button) findViewById(R.id.lokal);
        btnB = (Button) findViewById(R.id.sistemik);
        btnC = (Button) findViewById(R.id.upas);
        btnD = (Button) findViewById(R.id.kanker);
        btnE = (Button) findViewById(R.id.next);


        btnA.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Penyakits.this, Vsd.class);
                startActivity(intent);
            }
        });

        btnB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Penyakits.this, Bbuah.class);
                startActivity(intent);
            }
        });
        btnC.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Penyakits.this, Jupas.class);
                startActivity(intent);
            }
        });
        btnD.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Penyakits.this, Kanker.class);
                startActivity(intent);
            }
        });
//
////
//        btnB.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Intent intent = new Intent(Pilihdata.this, MainActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        btnC.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Intent intent = new Intent(Pilihdata.this, MainActivity.class);
//                startActivity(intent);
//            }
//        });
        btnE.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Penyakits.this, Penyakit2.class);
                startActivity(intent);
            }
        });

    }
}