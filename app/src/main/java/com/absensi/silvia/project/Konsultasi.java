package com.absensi.silvia.project;

/**
 * Created by silvia on 2/18/2018.
 */


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by silvia on 2/3/2018.
 */


public class Konsultasi extends Activity {


    Button btnA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.konsultasi);


        btnA = (Button) findViewById(R.id.mulai);

        btnA.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Konsultasi.this, Gejala.class);
                startActivity(intent);
            }
        });

    }
}