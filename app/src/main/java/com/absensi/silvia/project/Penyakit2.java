package com.absensi.silvia.project;

/**
 * Created by silvia on 4/1/2018.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by silvia on 2/3/2018.
 */


public class Penyakit2 extends Activity {


    Button btnA, btnB,btnC,btnD,btnE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.penyakit2);


        btnA = (Button) findViewById(R.id.lokal);
        btnB = (Button) findViewById(R.id.sistemik);
        btnC = (Button) findViewById(R.id.moniliasis);
        btnE = (Button) findViewById(R.id.back);


        btnA.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Penyakit2.this, Belangdaun.class);
                startActivity(intent);
            }
        });
        btnB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Penyakit2.this, Jamurakar.class);
                startActivity(intent);
            }
        });
        btnC.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Penyakit2.this, Moniliasis.class);
                startActivity(intent);
            }
        });
////
////
//        btnB.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Intent intent = new Intent(Pilihdata.this, MainActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        btnC.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Intent intent = new Intent(Pilihdata.this, MainActivity.class);
//                startActivity(intent);
//            }
//        });

        btnE.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Penyakit2.this, Penyakits.class);
                startActivity(intent);
            }
        });

        }
}