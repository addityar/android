package com.absensi.silvia.project;

/**
 * Created by silvia on 4/27/2018.
 */
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Frag2 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View frag2_pesan = inflater.inflate(R.layout.frag2, container, false);
        return frag2_pesan;
    }
}

