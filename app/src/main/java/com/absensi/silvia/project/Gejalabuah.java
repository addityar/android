package com.absensi.silvia.project;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

/**
 * Created by silvia on 4/19/2018.
 */

public class Gejalabuah extends Activity implements View.OnClickListener {

    Button hasil, selesai, lihat,so1,so2,so3,so4,so5,so6,so7,so8,so9,so10,so11,so12,so13,so14;
    EditText persentasi1, persentasi2, persentasi3, persentasi4, persentasi5, persentasi6, persentasi7,
            persentasi8, persentasi9, persentasi10, persentasi11, persentasi12, persentasi13, persentasi14;
    CheckBox a, b, c, d, e, f,g,h,i,j,k,l,m;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gejalabuah);

        a = (CheckBox) findViewById(R.id.cb1);
        b = (CheckBox) findViewById(R.id.cb2);
        c = (CheckBox) findViewById(R.id.cb3);
        d = (CheckBox) findViewById(R.id.cb4);
        e = (CheckBox) findViewById(R.id.cb5);
        f = (CheckBox) findViewById(R.id.cb6);
        g = (CheckBox) findViewById(R.id.cb7);
        h = (CheckBox) findViewById(R.id.cb8);
        i = (CheckBox) findViewById(R.id.cb9);
        j = (CheckBox) findViewById(R.id.cb10);
        k = (CheckBox) findViewById(R.id.cb11);
        l = (CheckBox) findViewById(R.id.cb12);
        m = (CheckBox) findViewById(R.id.cb13);

        persentasi1 = (EditText) findViewById(R.id.persentase1);
        persentasi2 = (EditText) findViewById(R.id.persentase2);
        persentasi3 = (EditText) findViewById(R.id.persentase3);
        persentasi4 = (EditText) findViewById(R.id.persentase4);
        persentasi5 = (EditText) findViewById(R.id.persentase5);
        persentasi6 = (EditText) findViewById(R.id.persentase6);
        persentasi7 = (EditText) findViewById(R.id.persentase7);
        persentasi8 = (EditText) findViewById(R.id.persentase8);
        persentasi9 = (EditText) findViewById(R.id.persentase9);
//        persentasi10 = (EditText) findViewById(R.id.persentase10);
//        persentasi11 = (EditText) findViewById(R.id.persentase11);
//        persentasi12 = (EditText) findViewById(R.id.persentase12);
        persentasi13 = (EditText) findViewById(R.id.persentase13);
        persentasi14 = (EditText) findViewById(R.id.persentase14);


        hasil = (Button) findViewById(R.id.hasil);
        hasil.setOnClickListener(this);

    }

    @Override
    public void onClick(View klik) {
        // TODO Auto-generated method stub


        if (a.isChecked() && b.isChecked()
                && c.isChecked() && d.isChecked() && e.isChecked() && f.isChecked()) {
            int ulat = (0);
            int kepik = (int)((3.0/6.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((3.0/6.0)*100);
            int antraknose = (0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));


        } else if (a.isChecked() && b.isChecked()
                && c.isChecked() && d.isChecked() && e.isChecked() && g.isChecked()) {
            int ulat = (0);
            int kepik = (int)((3.0/5.5)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((2.5/5.5)*100);
            int antraknose = (0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()
                && c.isChecked() && d.isChecked() && e.isChecked() && h.isChecked()) {
            int ulat = (0);
            int kepik = (int)((3.0/5.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((2.0/5.0)*100);
            int antraknose = (0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()
                && c.isChecked() && d.isChecked() && e.isChecked() && i.isChecked()) {
            int ulat = (0);
            int kepik = (int)((3.0/6.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((2.0/6.0)*100);
            int antraknose = (0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()
                && c.isChecked() && d.isChecked() && e.isChecked() && j.isChecked()) {
            int ulat = (0);
            int kepik = (int)((3.0/6.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((2.0/6.0)*100);
            int antraknose = (0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()
                && c.isChecked() && d.isChecked() && e.isChecked()&& l.isChecked()) {
            int ulat = (0);
            int kepik = (int)((3.0/6.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/6.0)*100);
            int antraknose = (0);
            int vsd = (0);
            int bbuah = (int)((1.0/6.0)*100);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));


        } else if (a.isChecked() && b.isChecked()
                && c.isChecked() && d.isChecked()) {
            int ulat = (0);
            int kepik = (int)((3.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/4.0)*100);
            int antraknose = (0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);



            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));


        } else if (a.isChecked() && b.isChecked()
                && c.isChecked()) {
            int ulat = (0);
            int kepik = (int)((3.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);



            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()
                && d.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/3.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/3.0)*100);
            int antraknose = (0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);



            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()
                && e.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/3.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/3.0)*100);
            int antraknose = (0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);



            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()
                && f.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/3.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/3.0)*100);
            int antraknose = (0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose = (0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);



            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && m.isChecked()
                ) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(int)((1.0/5.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);



            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()&& c.isChecked()&& d.isChecked()&& e.isChecked()
                && f.isChecked()&& g.isChecked()&& h.isChecked()&& i.isChecked()&& j.isChecked()&& k.isChecked()&& l.isChecked()&& m.isChecked()
                ) {
            int ulat = (0);
            int kepik = (int)((3.0/13.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((4.0/13.0)*100);
            int antraknose =(int)((1.0/13.0)*100);
            int vsd = (0);
            int bbuah = (int)((5.0/13.0)*100);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && f.isChecked()&& g.isChecked()&& h.isChecked()&& m.isChecked()
                ) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(int)((1.0/5.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked() && b.isChecked()
                && e.isChecked()&& c.isChecked()&& m.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((2.0/3.0)*100);
            int antraknose =(int)((1.0/3.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked() && b.isChecked()
                && f.isChecked()&& d.isChecked()&& e.isChecked()&& j.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((3.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked() && b.isChecked()&& j.isChecked()&& k.isChecked()&& m.isChecked()
                ) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(int)((1.0/5.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked() && b.isChecked()&& j.isChecked()&& k.isChecked()&& a.isChecked()
                ) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(int)((1.0/5.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked() && b.isChecked()
                && e.isChecked()&& l.isChecked()&& m.isChecked()&& c.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((2.0/3.0)*100);
            int antraknose =(int)((1.0/3.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && b.isChecked()
                && e.isChecked()&& h.isChecked()&& i.isChecked()&& m.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (int)((1.0/3.0)*100);
            int tikus = (int)((1.0/3.0)*100);
            int antraknose =(int)((1.0/3.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked() && b.isChecked()
                && c.isChecked()&& i.isChecked()&& f.isChecked()&& m.isChecked()&& k.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((2.0/3.0)*100);
            int antraknose =(int)((1.0/3.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && b.isChecked()
                && f.isChecked()&& k.isChecked()&& d.isChecked()&& e.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((2.0/5.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && b.isChecked()&& a.isChecked()&& d.isChecked()&& g.isChecked()&& h.isChecked())
        {
            int ulat = (0);
            int kepik = (int)((2.0/5.5)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (int)((1.5/5.5)*100);
            int tikus = (int)((2.0/5.5)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && b.isChecked()&& k.isChecked()&& l.isChecked()&& m.isChecked()&& i.isChecked()
                ) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(int)((1.0/2.0)*100);
            int vsd = (0);
            int bbuah = (int)((1.0/2.0)*100);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));
        } else if (c.isChecked() && b.isChecked()&& a.isChecked()&& j.isChecked()&& f.isChecked()&& l.isChecked()&& e.isChecked()
                ) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((3.0/5.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && b.isChecked()&& j.isChecked()&& m.isChecked()
                ) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(int)((3.0/5.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (b.isChecked() && c.isChecked() && d.isChecked()&& e.isChecked() && f.isChecked()&& k.isChecked()&& l.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (int)((1.0/4.0)*100);
            int tikus = (int)((3.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && d.isChecked() && e.isChecked() && f.isChecked()&& g.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (int)((1.0/4.0)*100);
            int tikus = (int)((3.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (g.isChecked() && e.isChecked() && f.isChecked()&& h.isChecked()&& i.isChecked()&& j.isChecked()&& k.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (int)((4.0/5.0)*100);
            int tikus = (int)((1.0/5.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if ( e.isChecked() && f.isChecked()&& a.isChecked()&& i.isChecked()&& m.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (int)((1.0/4.0)*100);
            int tikus = (int)((2.0/4.0)*100);
            int antraknose =(int)((1.0/4.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (f.isChecked()&& b.isChecked()&& a.isChecked()&& d.isChecked()&& k.isChecked()&& h.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (int)((1.0/4.0)*100);
            int tikus = (int)((2.0/4.0)*100);
            int antraknose =(int)((1.0/4.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);




            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked()&& i.isChecked()&& j.isChecked()&& k.isChecked()&& l.isChecked()&& m.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (int)((3.0/4.0)*100);
            int tikus = (0);
            int antraknose =(int)((1.0/4.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (b.isChecked()&& d.isChecked()&& f.isChecked()&& h.isChecked()&& k.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (int)((3.0/4.0)*100);
            int tikus = (0);
            int antraknose =(int)((1.0/4.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);



            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked()&& b.isChecked()&& j.isChecked()&& g.isChecked()&& i.isChecked()&& m.isChecked()&& f.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (int)((3.0/4.0)*100);
            int tikus = (int)((2.0/4.0)*100);
            int antraknose =(int)((1.0/4.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);



            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked()&& e.isChecked()&& h.isChecked()&& i.isChecked()&& j.isChecked()&& k.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (int)((3.0/4.0)*100);
            int tikus = (int)((2.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (e.isChecked()&& k.isChecked()&& l.isChecked()&& g.isChecked()&& f.isChecked()&& i.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (int)((3.0/4.0)*100);
            int tikus = (int)((2.0/4.0)*100);
            int antraknose =(int)((1.0/4.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (f.isChecked()&& g.isChecked()&& h.isChecked()&& i.isChecked()&& a.isChecked()&& c.isChecked()&& m.isChecked()&& d.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (int)((3.0/4.0)*100);
            int tikus = (int)((2.0/4.0)*100);
            int antraknose =(int)((1.0/4.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked()&& d.isChecked()&& k.isChecked()&& l.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (int)((1.0/4.0)*100);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (e.isChecked()&& f.isChecked()&& b.isChecked()&& m.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(int)((1.0/4.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked()&& h.isChecked()&& m.isChecked()&& g.isChecked()&& b.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(int)((1.0/4.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (b.isChecked()&& k.isChecked()&& l.isChecked()&& m.isChecked()&& c.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(int)((1.0/4.0)*100);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()
                && c.isChecked() && d.isChecked() && e.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()
                && c.isChecked() && d.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);
            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()
                && c.isChecked()) {
            int ulat = (0);
            int kepik = (int)((3.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked()) {
            int ulat = (0);
            int kepik = (int)((1.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));


        } else if (b.isChecked()) {
            int ulat = (0);
            int kepik = (int)((1.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));


        } else if (c.isChecked()) {
            int ulat = (0);
            int kepik = (int)((1.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (e.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (f.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);
            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked()&& d.isChecked()) {
            int ulat = (0);
            int kepik = (int)((1.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked() && e.isChecked()
                ) {
            int ulat = (0);
            int kepik = (int)((1.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);



            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked() && f.isChecked()
                ) {
            int ulat = (0);
            int kepik = (int)((1.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (b.isChecked() && c.isChecked() && d.isChecked() && e.isChecked() && f.isChecked()
                ) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);



            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked() && c.isChecked()
                && e.isChecked()&& f.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((3.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked() && e.isChecked()
                && f.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((3.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (f.isChecked() && e.isChecked()
                ) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((3.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && c.isChecked()
                ) {
            int ulat = (0);
            int kepik = (int)((1.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && b.isChecked()
                && d.isChecked()) {

            int ulat = (0);
            int kepik = (int)((1.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && b.isChecked()
                && e.isChecked()) {

            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked() && b.isChecked()
                && c.isChecked()) {

            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && b.isChecked()
                && f.isChecked()) {

            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && b.isChecked())
        {

            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && f.isChecked()
                ) {

            int ulat = (0);
            int kepik = (int)((1.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));
        } else if (c.isChecked() && b.isChecked()
                ) {

            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked() && e.isChecked()
                ) {

            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && b.isChecked() && d.isChecked()&& e.isChecked() && f.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked() && d.isChecked() && e.isChecked() && f.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked() && e.isChecked() && a.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if ( a.isChecked() && b.isChecked() && e.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/3.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/3.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (b.isChecked()&& a.isChecked()&& d.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/3.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/3.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);



            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (b.isChecked()&& f.isChecked()) {
            int ulat = (0);
            int kepik = (int)((3.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked()&& b.isChecked()&& f.isChecked()) {
            int ulat = (0);
            int kepik = (int)((3.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked()&& d.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((3.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (d.isChecked()&& b.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((3.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (e.isChecked()&& a.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((3.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (f.isChecked()&& b.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((1.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (a.isChecked()&& b.isChecked()&& f.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (e.isChecked()&& b.isChecked()&& d.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (c.isChecked()&& b.isChecked()&& e.isChecked()) {
            int ulat = (0);
            int kepik = (int)((2.0/4.0)*100);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (0);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);


            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));

        } else if (b.isChecked()&& e.isChecked()&& d.isChecked()) {
            int ulat = (0);
            int kepik = (0);
            int kumbang = (0);
            int pbatang = (0);
            int pbuah = (0);
            int tikus = (int)((2.0/4.0)*100);
            int antraknose =(0);
            int vsd = (0);
            int bbuah = (0);
            int jupas = (0);
            int jakar = (0);

            persentasi1.setText(String.valueOf(ulat + "%"));
            persentasi2.setText(String.valueOf(kepik + "%"));
            persentasi3.setText(String.valueOf(kumbang + "%"));
            persentasi4.setText(String.valueOf(pbatang + "%"));
            persentasi5.setText(String.valueOf(pbuah + "%"));
            persentasi6.setText(String.valueOf(tikus + "%"));
            persentasi7.setText(String.valueOf(vsd + "%"));
            persentasi8.setText(String.valueOf(jupas + "%"));
            persentasi9.setText(String.valueOf(jakar + "%"));
//            persentasi10.setText(String.valueOf(moniliasis + "%"));
//            persentasi11.setText(String.valueOf(belang + "%"));
//            persentasi12.setText(String.valueOf(kanker + "%"));
            persentasi13.setText(String.valueOf(antraknose + "%"));
            persentasi14.setText(String.valueOf(bbuah + "%"));



        }


        a.setChecked(false);
        b.setChecked(false);
        c.setChecked(false);
        d.setChecked(false);
        e.setChecked(false);
        f.setChecked(false);
        g.setChecked(false);
        h.setChecked(false);
        i.setChecked(false);
        j.setChecked(false);
        k.setChecked(false);
        l.setChecked(false);
        m.setChecked(false);
        so1 = (Button) findViewById(R.id.s1);
        so2 = (Button) findViewById(R.id.s2);
        so3 = (Button) findViewById(R.id.s3);
        so4 = (Button) findViewById(R.id.s4);
        so5 = (Button) findViewById(R.id.s5);
        so6 = (Button) findViewById(R.id.s6);
        so7 = (Button) findViewById(R.id.s7);
        so8 = (Button) findViewById(R.id.s8);
        so9 = (Button) findViewById(R.id.s9);
        so10 = (Button) findViewById(R.id.s10);
        so11 = (Button) findViewById(R.id.s11);
        so12 = (Button) findViewById(R.id.s12);
        so13 = (Button) findViewById(R.id.s13);
        so14 = (Button) findViewById(R.id.s14);

//        selesai.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Gejalabatang.this, Utama.class);
//                startActivity(intent);
//
//            }
//        });
//        lihat.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Gejalabatang.this, Pengendalian.class);
//                startActivity(intent);
//
//            }
//        });
        so1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejalabuah.this, Ulat.class);
                startActivity(intent);

            }
        });
        so2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejalabuah.this, Quest69.class);
                startActivity(intent);

            }
        });
        so3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejalabuah.this, Quest5.class);
                startActivity(intent);

            }
        });
        so4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejalabuah.this, Quest2.class);
                startActivity(intent);

            }
        });
        so5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejalabuah.this, Quest3.class);
                startActivity(intent);

            }
        });
        so6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejalabuah.this, Quest6.class);
                startActivity(intent);

            }
        });
        so7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejalabuah.this, Quest41.class);
                startActivity(intent);

            }
        });
        so8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejalabuah.this, Quest17.class);
                startActivity(intent);

            }
        });
        so9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejalabuah.this, Quest7.class);
                startActivity(intent);

            }
        });
//        so10.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Gejaladaun.this, Monili.class);
//                startActivity(intent);
//
//            }
//        });
//        so11.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Gejaladaun.this, Belang.class);
//                startActivity(intent);
//
//            }
//        });
//        so12.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Gejaladaun.this, Kankerba.class);
//                startActivity(intent);
//
//            }
//        });
        so13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejalabuah.this, Quest1.class);
                startActivity(intent);

            }
        });
        so14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gejalabuah.this, Questg1.class);
                startActivity(intent);

            }
        });

    }
}
