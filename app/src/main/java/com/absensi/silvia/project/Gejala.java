package com.absensi.silvia.project;

/**
 * Created by silvia on 2/18/2018.
 */


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by silvia on 2/3/2018.
 */


public class Gejala extends Activity {


    Button btnA, btnB,btnC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gejala);


        btnA = (Button) findViewById(R.id.lokal);
        btnB = (Button) findViewById(R.id.sistemik);
        btnC = (Button) findViewById(R.id.back);

        btnA.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Gejala.this, Glokal.class);
                startActivity(intent);
            }
        });
//
//
        btnB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Gejala.this, Gsistemik.class);
                startActivity(intent);
            }
        });
        btnC.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Gejala.this, Utama.class);
                startActivity(intent);
            }
        });

    }
}