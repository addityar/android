package com.absensi.silvia.project;

/**
 * Created by silvia on 2/26/2018.
 */


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by root on 11/13/17.
 */

public class RecyclerViewPenyakit extends RecyclerView.Adapter <RecyclerViewPenyakit.MyHolder>{

    String nama [] = {"Busuk Buah", "Penggerek Buah Kakao", "Penggerek Batang ", "Kutu Putih","Ulat Kantong","Ulat Kilan","Ulat Matahari","Ulat Jaran","Lalat Buah"};
    int gambar [] = {R.drawable.penghisap,
            R.drawable.penggerek,
            R.drawable.ulat,R.drawable.kutu,
            R.drawable.ulatkantong, R.drawable.ulatkilan, R.drawable.ulatpilan, R.drawable.ulatjaran, R.drawable.lalatbuah};
    String detail [] = {"Kepik penghisap buah kakao aau kepik Helopeltis sp. menyerang buah kakao dan pucuk / ranting muda tanaman kakao","Penggerek buah kakao (PBK) atau Conopomorpha cramella merupakan hama yang menyerang buah kakao. Hama ini sangat merugukan karena serangannya dapat merusak hampir semua hasil. Ulat hama ini merusak dengan menggerek buah, memakan kulit buah, daging buah dan saluran biji.",
            "Terdapat 2 hama penggerek batang, yaitu Zeuzera coffeae dan Glenea sp. Penggerek baang Zeuzra Coffeae memiliki bentuk seperti ulat yang merusak bagian batang / cabang dengan cara menggerek batang.cabang. Hama ini menyerang tanaman muda. Sedangkan penggerek batang Glenea sp. menyerang batang utama dan pangkal cabang utama dengan arah gerakan menyamping.",
            "Hama yang satu ini menyerang buah kakao yang masih kecil, bagian buah yang pertama adalah bagian pangkal buah selanjutnya menjalar kebagian buah lainnya, buah yang terserang hama ini akan memiliki pertumbuhan yang terhambat kemudian buah tersebut kering dan mati.\n" +
                    "\n" +
                    "Pengendalian hama ini dapat dilakukan dengan cara memangkas bagian yang terserang hama lalu membakarnya, bisa juga dengan melepaskan predator alaminya seperti Scymus sp, semut hitam atau parasit Coccophagus preudococci, atau bisa juga dengan menyemprotkan bahan kimia.",
            "Ulat kantong termasuk dalam famili Psychidae. Tujuh spesies yang pernah ditemukan pada tanaman kelapa sawit adalah Metisa plana, Mahasena corbetti, Cremastopsyche pendula, Brachycyttarus griseus, Manatha albipes, Amatissa sp. dan Cryptothelea cardiophaga (Norman et al., 1995). Jenis ulat kantong yang paling merugikan di perkebunan kelapa sawit adalah Metisa plana dan Mahasena corbetti.\n" +
                    "Siklus Hidup dan biologinya\n" +
                    "Ciri khas ulat kantong adalah hidupnya di dalam sebuah bangunan mirip kantong yang berasal dari potongan-potongan daun, tangkai bunga tanaman inang, di sekitar daerah serangan (Norman et al., 1995). Ciri khas yang lain yakni pada bagian tubuh dewasa betina kebanyakan spesies ulat kantong mereduksi dan tidak mampu untuk terbang. Jantan memiliki sayap dan akan mencari betina karena bau feromon yang dikeluarkan betina untuk menarik serangga jantan.",
            "Ulat Kilan (Hyposidea infixaria) merupakan hama yang termasuk dalam famili Geometridae. Hama ini menyerang pada saat tanaman berumur sekitar 2 hingga 4 bulan, ham ini memakan daun muda tanaman kakao dan yang disisakan hanya bagian tulang daunnya saja.\n" +
                    "\n" +
                    "Pengendalian hama ini dapat dilakukan dengan cara menyemprotan insektisida.","Ulat matahari merupakan hama yang menyerang pada bagian daun muda, kuncup daun dan juga bunga kakao yang masih muda. Spesies ulat matahari yang sering menyerang tanaman kakao yaitu Parasa lepida dan Ploneta diducta.","Hama yang menyerang tanaman kakao ini merupakan anggota dari familiki Limanthriidae. Hama ini memiliki bulu gatal pada bagian dorsal mirip seperti rambut pada leher kuda.\n" +
            "\n" +
            "Pengendalian hama ini dapat dilakukan dengan melepaskan predator alami ulat ini yaitu Apanteles mendosa dan Carcelia spp atau juga dapat dengan melakukan penyemprotan menggunakan insektisida kimia.","Hama ini merupakan anggota dari famili Lithocolletidae. Hama ini menyerang bagian buah, buah kakao yang diserang adalah kakao yang masih muda. Buah yang terserang hama ini akan memiliki kulit buah berwarna kuning pucat, biji tidak mengembang dan juga lengket.\n" +
            "\n" +
            "Pengendalian hama ini dapat dilakukan dengan cara melakukan sanitasi kebun, menyelumbungi buah dengan plastik atau yang lainnya dengan bagian bawah terbuka, melepaskan predator alami hama ini seperti semut hitam dan juga jamur antagonis Beauveria bassiana dengan cara di semprotkan."};

    Context context;
    LayoutInflater layoutInflater;

    public RecyclerViewPenyakit (Context context ){

        this.context = context;
        layoutInflater = layoutInflater.from(context);

    }
    @Override
    public RecyclerViewPenyakit.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.list_penyakit, parent, false);
        MyHolder holder = new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewPenyakit.MyHolder holder, final int position) {

        // ini berfungsi untuk mengirim data ke DetailActivity
        holder.txt.setText(nama [position]);
        holder.img.setImageResource(gambar [position]);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent kirimData = new Intent(context, ScrollingActivity.class);
                kirimData.putExtra("Nama", nama [position]);
                kirimData.putExtra("IMG", gambar [position]);
                kirimData.putExtra("DET", detail [position]);

                context.startActivity(kirimData);

            }
        });
    }

    @Override
    public int getItemCount() {
        return nama.length;
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        // ID ini diambil dari file list_item.xml
        ImageView img;
        TextView txt;

        public MyHolder(View itemView) {

            super(itemView);
            img = (ImageView)itemView.findViewById(R.id.imgbaris);
            txt = (TextView)itemView.findViewById(R.id.txtbaris);
        }
    }
}

