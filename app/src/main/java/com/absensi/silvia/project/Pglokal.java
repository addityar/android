package com.absensi.silvia.project;

/**
 * Created by silvia on 2/18/2018.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by silvia on 2/3/2018.
 */


public class Pglokal extends Activity {


    Button btnA, btnB,btnC,btnD,btnE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pilih_glokal);


        btnA = (Button) findViewById(R.id.batang);
        btnB = (Button) findViewById(R.id.buah);
        btnC = (Button) findViewById(R.id.daun);
        btnD = (Button) findViewById(R.id.ranting);
        btnE = (Button) findViewById(R.id.back);


        btnA.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Pglokal.this, Gejalabatang.class);
                startActivity(intent);
            }
        });
//
//
        btnB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Pglokal.this, Gejalabuah.class);
                startActivity(intent);
            }
        });

        btnC.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Pglokal.this, Gejaladaun.class);
                startActivity(intent);
            }
        });

        btnD.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Pglokal.this, Gejalaranting.class);
                startActivity(intent);
            }
        });

        btnE.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Pglokal.this, Utama.class);
                startActivity(intent);
            }
        });

    }
}