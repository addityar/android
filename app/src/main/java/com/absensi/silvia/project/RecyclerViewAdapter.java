package com.absensi.silvia.project;

/**
 * Created by silvia on 2/18/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by root on 11/13/17.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter <RecyclerViewAdapter.MyHolder>{

    String nama [] = {"Kepik Penghisap", "Penggerek Buah Kakao", "Penggerek Batang ", "Ulat Api","Tikus","Kumbang Apoginia Sp","Ulat Kilan"};
    int gambar [] = {R.drawable.penghisap,
            R.drawable.penggerek,
            R.drawable.ulat,R.drawable.ulatjaran,
            R.drawable.tikuss,R.drawable.kumbangapoginia, R.drawable.ulatkilan};
    String detail [] = {
            "Kepik penghisap buah kakao atau kepik Helopeltis sp. menyerang buah kakao dan pucuk / ranting muda tanaman kakao\n" +"\n" + "Kepik Penghisap Buah\n" +
            "Filum : Arthropoda\n" +
            "Kelas : Insecta\n" +
            "Ordo : Hemiptera\n" +
            "Famili : Miridae\n" +
            "Genus : Helopeltis\n" +
            "Spesies: Helopeltis sp\n" +
            "(Kalshoven, 1981).\n" + "\n"+
            "Serangan\n" +
            "1. Serangan terjadi pada buah muda\n" +
            "2. Pada buah tua tidak terlalu merugikan\n\n\n" +
            "Gejala\n" + "\n"+
            "1. Buah muda yang terserang mengering dan rontok\n" +
            "2. Jika tumbuh, permukaan kulit buah akan retak\n" +
            "3. Buah Nampak bercak-bercak coklat kehitaman\n",
            "Penggerek buah kakao (PBK) atau Conopomorpha cramella merupakan hama yang menyerang buah kakao. Hama ini sangat merugukan karena serangannya dapat merusak hampir semua hasil. Ulat hama ini merusak dengan menggerek buah\n"+" Penggerek Buah Kakao\n" +"\n"+
            "Filum : Arthropoda\n" +
            "Kelas : Insecta\n" +
            "Ordo : Lepidoptera\n" +
            "Famili : Gracillariidae\n" +
            "Genus : Conopomorpha\n" +
            "Spesies :\n" +
            "Conopomorpha\n" +
            "cramerella\n" +
            "(Kalshoven, 1981).\n" +
            "Serangan\n" +
            "1. Menyerang buah muda dengan diameter 3 cm sampai dengan 8 cm.\n" +
            "2. Ulatnya merusak dengan menggerek\n" +
            "3. Ulatnya memakan kulit, daging buah dan saluran ke biji\n" +
            "(Pracaya, 2009)\n" +
            "Gejala\n" +
            "1. Buah akan lebih awal berwarna kuning\n" +
            "2. Jika digoyangkan tidak berbunyi\n" +
            "3. Buah lebih berat\n" +
            "4. Biji-biji saling Melekat.\n",
            "Penggerek Batang\n" +
                    "Filum : Arthropoda\n" +
                    "Kelas : Insecta\n" +
                    "Ordo : Lepidoptera\n" +
                    "Famili : Cossidae\n" +
                    "Genus : Zeuzera\n" +
                    "Spesies: Zeuzera coffeae\n" +
                    "(Kalshoven, 1981).\n\n" +
                    "Serangan\n" +
                    "1. Merusak bagian batang atau cabang\n" +
                    "2. Menggerek menuju empelur (Xilem) batang/ cabang\n" +
                    "3. Gerekan membelok keatas dan menyerang tanaman muda (Pracaya, 2009).\n\n" +
                    "Gejala\n" +
                    "1. Batang menjadi layu\n" +
                    "2. Batang menjadi kering\n" +
                    "3. Batang menjadi mati\n",
            "Ulat Api\n\n" +
                    "Filum : Arthropoda\n" +
                    "Kelas : Insecta\n" +
                    "Ordo : Hemiptera\n" +
                    "Famili : Limacodidae\n" +
                    "Genus : Darna\n" +
                    "Spesies : Darna Trina\n" +
                    "(Kalshoven, 1981).\n\n" +
                    "Serangan\n" +
                    "1.Menyerang daun kakao dengan memakan daging daun.\n" +
                    "2. Setelah ulat besar, akan memakan seluruh jaringan daun\n" +
                    "(Rukmana & Yudirachman, 2016). \n\n" +
                    "Gejala\n" +
                    "1. Daunberbintikbintik transparan\n" +
                    "2. Daun menguning\n" +
                    "3. Daun mengering\n" +
                    "4. Daun berlubang\n",
            "Tikus\n\n" +
                    "Filum : Chordata\n" +
                    "Kelas : Mammalia\n" +
                    "Ordo : Rodentia\n" +
                    "Famili : Muridae\n" +
                    "Genus : Rattus\n" +
                    "Spesies:Rattus argentiventer\n" +
                    "(Kalshoven, 1981).\n\n" +
                    "Serangan\n\n" +
                    "1. Serangan tikus yaitu memakan buah kakao\n" +
                    "2. Buah yang dimakan akan berlubang dan kemasukan air sehingga terserang bakteri dan jamur\n" +
                    "(Pracaya, 2009).\n\n" +
                    "Gejala\n\n" +
                    "1. Buah menjadi berlubang\n" +
                    "2. Buah menjadi busuk\n" +
                    "3. Kerusakan sering terjadi pada buah yang sudah matang\n" +
                    "4. Kerusakan terjadi pada buah yang sudah matang\n",
                    "Kumbang Apoginia sp\n\n" +
                    "Filum : Arthropoda\n" +
                    "Kelas : Insecta\n" +
                    "Ordo : Coleoptera\n" +
                    "Famili : Scrabaeidae\n" +
                    "Genus : Apogonia\n" +
                    "Spesies : Apogonia sp\n" +
                    "(Kalshoven, 1981).\n\n" +
                    "Serangan\n\n" +
                    "1. Menyerang daun muda pada malam hari\n" +
                    "2. Memakan daun mulai dari pinggir\n" +
                    "3. Tingkat serangan berhubungan dengan kerapatan pohon pelindungnya\n" +
                    "(Pracaya, 2009).\n\n" +
                    "Gejala\n\n" +
                    "1. Daun berlubang\n" +
                    "2. Kerusakan yang terjadi pada bagian pinggir daun\n","Ulat Kilan\n\n" +
            "Filum : Arthropoda\n" +
            "Kelas : Insecta\n" +
            "Ordo : Lepidoptera\n" +
            "Famili : Geometridae\n" +
            "Genus : Hyposidra\n" +
            "Spesies: Hyposidra talaca\n" +
            "(Kalshoven, 1981).\n\n" +
            "Serangan\n" +
            "1. Ulat kilan menyerang daun, bunga, dan pentil kakao (Rukmana & Yudirachman, 2016).\n\n" +
            "Gejala\n" +
            "1. Daun berlubang\n" +
            "2. Pucuk Tanaman gundul sehingga tinggal tulang dan daun saja.\n"

           };

    Context context;
    LayoutInflater layoutInflater;

    public RecyclerViewAdapter (Context context ){

        this.context = context;
        layoutInflater = layoutInflater.from(context);

    }
    @Override
    public RecyclerViewAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.list_hama, parent, false);
        MyHolder holder = new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.MyHolder holder, final int position) {

        // ini berfungsi untuk mengirim data ke DetailActivity
        holder.txt.setText(nama [position]);
        holder.img.setImageResource(gambar [position]);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent kirimData = new Intent(context, DetailActivity.class);
                kirimData.putExtra("Nama", nama [position]);
                kirimData.putExtra("IMG", gambar [position]);
                kirimData.putExtra("DET", detail [position]);

                context.startActivity(kirimData);

            }
        });
    }

    @Override
    public int getItemCount() {
        return nama.length;
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        // ID ini diambil dari file list_item.xml
        ImageView img;
        TextView txt;

        public MyHolder(View itemView) {

            super(itemView);
            img = (ImageView)itemView.findViewById(R.id.imgbaris);
            txt = (TextView)itemView.findViewById(R.id.txtbaris);
        }
    }
}

